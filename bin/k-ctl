#!/usr/bin/env bash

##############################
# wrapper script for running
# helm against this chart

set -euf -o pipefail
dir="$(cd "$(dirname "${0}")"; pwd)"

NAME="${NAME:-gitlab-monitoring}"
CHART="${CHART:-prometheus-operator}"
NAMESPACE="${NAMESPACE:-monitoring}"

# Source common functions and variable exports
# that are common to all charts

COMMON_SCRIPT_PATH="${COMMON_SCRIPT_PATH:-/k8s-workloads/common.bash}"
common_registry_image="registry.gitlab.com/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:latest"
if [[ -r "$COMMON_SCRIPT_PATH" ]]; then
    source "$COMMON_SCRIPT_PATH"
else
    echo "Extracting common.bash from $common_registry_image..."
    docker pull "$common_registry_image" >/dev/null
    tmpfile=$(mktemp)
    container_id=$(docker create $common_registry_image)
    docker cp "$container_id:$COMMON_SCRIPT_PATH" "$tmpfile"
    source "$tmpfile"
    rm -f "$tmpfile"
fi

declare -r encrypted_file='k8s_alertmanager.yaml.enc'
declare -r decrypted_file='alertmanager.yaml'
declare -r bucket='gitlab-configs'
declare -r kms_keyring='gitlab-shared-configs'
declare -r kms_key='config'

charts_dir=$(mktemp -d)
manifests_dir="$dir/../manifests"

get_alertmanager_config() {
    echo -ne "${_CYN}Getting alertmanager genereated configuration...${_NORM}"
    gsutil cp gs://${bucket}/${encrypted_file} ./
    gcloud kms decrypt \
      --location global \
      --project gitlab-ops \
      --keyring "${kms_keyring}" \
      --key "${kms_key}" \
      --ciphertext-file "${encrypted_file}" \
      --plaintext-file "${decrypted_file}"
}

prometheus_static_ip=$(get_static_ip "prometheus-gke-${ENV}")
thanos_query_static_ip=$(get_static_ip "thanos-query-gke-${ENV}")

export THANOS_QUERY_STATIC_IP="${thanos_query_static_ip}"

helm_opts_values=(
    "${HELM_OPTS_VALUES[@]}"
    "-f" "alertmanager-templates.yaml"
    "-f" "service-monitors.yaml"
    "-f" "${decrypted_file}"
    "--set" "prometheus.service.loadBalancerIP=$prometheus_static_ip"
    "--set" "prometheus.prometheusSpec.externalLabels.env=${ENV}"
    "--set" "prometheus.prometheusSpec.externalLabels.region=${REGION}"
)

kubectl_opts=(
    "${KUBECTL_OPTS[@]}"
    "--namespace" "${NAMESPACE}"
)

case "$ACTION" in
    install|upgrade)
        get_alertmanager_config
        helm_opts=(
            "stable/$CHART"
            "${HELM_OPTS[@]}"
            "${helm_opts_values[@]}"
        )
        if [[ $ACTION == "install" ]]; then
            set -x
            helm tiller run helm install --name "$NAME" --namespace "${NAMESPACE}" "${helm_opts[@]}"
            envsubst < thanos-query-components.yaml | kubectl apply "${kubectl_opts[@]}" --filename -
        else
            set -x
            helm tiller run helm upgrade --namespace "${NAMESPACE}" "$NAME" "${helm_opts[@]}"
            envsubst < thanos-query-components.yaml | kubectl apply "${kubectl_opts[@]}" --filename -
        fi
        ;;

    list)
        set -x
        helm tiller run helm list

        kubectl get --namespace "${NAMESPACE}" --filename thanos-query-components.yaml
        ;;

    remove)
        echo -ne "${_BRED}!! You are about to remove the $CHART chart on $CLUSTER in $ENV ($PROJECT) !!${_NORM}\\n"
        echo -ne "press enter to continue."
        read -rp ""
        echo -e "${_CYN}continuing...${_NORM}"

        helm_opts=(
            "--purge" "$NAME"
            "${HELM_OPTS[@]}"
        )

        set -x
        helm tiller run helm del "${helm_opts[@]}"
        ## Uninstalling the prometheus operator helm chart requires some extra
        ## cleanup.
        ## https://github.com/helm/charts/tree/master/stable/prometheus-operator#uninstalling-the-chart
        set +x
        for item in prometheuses.monitoring.coreos.com \
                    prometheusrules.monitoring.coreos.com \
                    podmonitors.monitoring.coreos.com \
                    servicemonitors.monitoring.coreos.com \
                    alertmanagers.monitoring.coreos.com; do
            if [[ ${dry_run:-} != "true" ]]; then
              set -x
              kubectl delete "${kubectl_opts[@]}" crd "$item"
            else
              set +x
              echo "Would run: kubectl delete ${kubectl_opts[*]} crd $item"
            fi
        done
        if [[ ${dry_run:-} != "true" ]]; then
          set -x
          kubectl delete "${kubectl_opts[@]}" --filename thanos-query-components.yaml
          kubectl delete secret thanos-objstore-config "${kubectl_opts[@]}"
          set +x

          echo -e "${_BRED}Ensure all Persistent Volumes and Persistent Volume Claims are cleaned up!${_NORM}"
        else
          set +x
          echo "Would run: kubectl delete ${kubectl_opts[*]} --filename thanos-query-components.yaml"
        fi
        ;;

    template)
        get_alertmanager_config
        helm_opts=(
            "$charts_dir/prometheus-operator"
            "--output-dir" "$manifests_dir"
            "--namespace" "$NAMESPACE"
            "${HELM_OPTS[@]}"
            "${helm_opts_values[@]}"
        )
        set -x
        rm -rf "${manifests_dir:?}/*"
        helm fetch stable/prometheus-operator --untar --untardir "$charts_dir"
        helm tiller run helm template --namespace "${NAMESPACE}" "${helm_opts[@]}" "$@"

        envsubst < thanos-query-components.yaml > "${manifests_dir}"/thanos-query-components.yaml
esac

set +x
rm -rvf "${encrypted_file:?}"
rm -rvf "${decrypted_file:?}"
rm -rf "${charts_dir:?}"
